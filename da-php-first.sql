-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5332
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for da-php-first
CREATE DATABASE IF NOT EXISTS `da-php-first` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `da-php-first`;

-- Dumping structure for table da-php-first.banner
CREATE TABLE IF NOT EXISTS `banner` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `HINH` varchar(256) NOT NULL,
  `RONG` varchar(256) NOT NULL,
  `CAO` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table da-php-first.banner: ~0 rows (approximately)
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
INSERT INTO `banner` (`ID`, `HINH`, `RONG`, `CAO`) VALUES
	(1, 'banner.jpg', '990px', '150px');
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;

-- Dumping structure for table da-php-first.footer
CREATE TABLE IF NOT EXISTS `footer` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `HTML` mediumtext,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table da-php-first.footer: ~0 rows (approximately)
/*!40000 ALTER TABLE `footer` DISABLE KEYS */;
INSERT INTO `footer` (`ID`, `HTML`) VALUES
	(1, '<table width="990px">\r\n    <tr>\r\n        <td width="495px" align="right" >\r\n       Cua hang : \r\n        </td>\r\n        <td width="495px" >\r\n        Shop abc\r\n        </td>\r\n    </tr>\r\n    <tr>\r\n        <td align="right" >\r\n        Dien thoai : \r\n        </td>\r\n        <td>\r\n        so_dien_thoai\r\n        </td>\r\n    </tr>\r\n    <tr>\r\n        <td align="right">\r\n        Dia chi : \r\n        </td>\r\n        <td>\r\n        dia_chi\r\n        </td>\r\n    </tr>\r\n</table>\r\n');
/*!40000 ALTER TABLE `footer` ENABLE KEYS */;

-- Dumping structure for table da-php-first.hoa_don
CREATE TABLE IF NOT EXISTS `hoa_don` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TEN_NGUOI_MUA` varchar(20) NOT NULL,
  `EMAIL` varchar(255) NOT NULL,
  `DIA_CHI` char(25) DEFAULT NULL,
  `DIEN_THOAI` int(11) DEFAULT NULL,
  `NOI_DUNG` mediumtext,
  `HANG_DUOC_MUA` mediumtext,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table da-php-first.hoa_don: ~0 rows (approximately)
/*!40000 ALTER TABLE `hoa_don` DISABLE KEYS */;
/*!40000 ALTER TABLE `hoa_don` ENABLE KEYS */;

-- Dumping structure for table da-php-first.menu_doc
CREATE TABLE IF NOT EXISTS `menu_doc` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TEN` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table da-php-first.menu_doc: ~6 rows (approximately)
/*!40000 ALTER TABLE `menu_doc` DISABLE KEYS */;
INSERT INTO `menu_doc` (`ID`, `TEN`) VALUES
	(1, 'menu1'),
	(2, 'menu2'),
	(3, 'menu3'),
	(4, 'menu4'),
	(5, 'menu5'),
	(6, 'menu6');
/*!40000 ALTER TABLE `menu_doc` ENABLE KEYS */;

-- Dumping structure for table da-php-first.menu_ngang
CREATE TABLE IF NOT EXISTS `menu_ngang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TEN` varchar(256) DEFAULT NULL,
  `NOI_DUNG` mediumtext,
  `LOAI_MENU` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table da-php-first.menu_ngang: ~57 rows (approximately)
/*!40000 ALTER TABLE `menu_ngang` DISABLE KEYS */;
INSERT INTO `menu_ngang` (`ID`, `TEN`, `NOI_DUNG`, `LOAI_MENU`) VALUES
	(20, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(21, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(22, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(23, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(24, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(25, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(26, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(27, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(28, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(29, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(30, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(31, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(32, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(33, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(34, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(35, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(36, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(37, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(38, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(39, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(40, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(41, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(42, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(43, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(44, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(45, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(46, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(47, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(48, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(49, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(50, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(51, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(52, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(53, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(54, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(55, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(56, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(57, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(58, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(59, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(60, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(61, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(62, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(63, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(64, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(65, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(66, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(67, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(68, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(69, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(70, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(71, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(72, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(73, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(74, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(75, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3'),
	(76, 'MENU1', 'HHHHHHHHHHHHHHHHHHHHHHHHHH', '3');
/*!40000 ALTER TABLE `menu_ngang` ENABLE KEYS */;

-- Dumping structure for table da-php-first.quang_cao
CREATE TABLE IF NOT EXISTS `quang_cao` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `HTML` mediumtext,
  `VI_TRI` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table da-php-first.quang_cao: ~2 rows (approximately)
/*!40000 ALTER TABLE `quang_cao` DISABLE KEYS */;
INSERT INTO `quang_cao` (`ID`, `HTML`, `VI_TRI`) VALUES
	(1, 'noi dung quang cao', 'trai'),
	(2, 'noi dung quang cao', 'phai');
/*!40000 ALTER TABLE `quang_cao` ENABLE KEYS */;

-- Dumping structure for table da-php-first.san_pham
CREATE TABLE IF NOT EXISTS `san_pham` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TEN` varchar(256) NOT NULL,
  `GIA` int(255) NOT NULL,
  `HINH_ANH` varchar(256) DEFAULT NULL,
  `NOI_DUNG` mediumtext,
  `THUOC_MENU` int(255) DEFAULT NULL,
  `NOI_BAT` varchar(256) DEFAULT NULL,
  `TRANG_CHU` varchar(256) DEFAULT NULL,
  `SAP_XEP_TRANG_CHU` int(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table da-php-first.san_pham: ~20 rows (approximately)
/*!40000 ALTER TABLE `san_pham` DISABLE KEYS */;
INSERT INTO `san_pham` (`ID`, `TEN`, `GIA`, `HINH_ANH`, `NOI_DUNG`, `THUOC_MENU`, `NOI_BAT`, `TRANG_CHU`, `SAP_XEP_TRANG_CHU`) VALUES
	(1, 'SAN PHAM 1', 500, '1.jpg', 'HIHI', 1, NULL, 'co', NULL),
	(2, 'SAN PHAM 12', 500, '1.jpg', 'HIHI', 1, NULL, NULL, NULL),
	(3, 'SAN PHAM 13', 500, '1.jpg', 'HIHI', 3, NULL, NULL, NULL),
	(4, 'SAN PHAM 14', 500, '1.jpg', 'HIHI', 1, NULL, NULL, NULL),
	(5, 'SAN PHAM 15', 500, '1.jpg', 'HIHI', 1, NULL, NULL, NULL),
	(6, 'SAN PHAM 16', 500, '1.jpg', 'HIHI', 1, NULL, NULL, NULL),
	(7, 'SAN PHAM 17', 500, '1.jpg', 'HIHI', 1, 'co', 'co', NULL),
	(8, 'SAN PHAM 1433', 500, '1.jpg', 'HIHI', 1, NULL, NULL, NULL),
	(9, 'SAN PHAM 4321', 500, '1.jpg', 'HIHI', 1, NULL, NULL, NULL),
	(10, 'SAN PHAM3 1', 500, '1.jpg', 'HIHI', 1, NULL, NULL, NULL),
	(11, 'SAN PHAM 431', 500, '1.jpg', 'HIHI', 1, NULL, NULL, NULL),
	(12, 'SAN PHAM 53241', 500, '1.jpg', 'HIHI', 1, 'co', 'co', NULL),
	(13, 'SAN 51', 500, '1.jpg', 'HIHI', 1, 'co', NULL, NULL),
	(14, 'SAN PHAM 2341', 500, '1.jpg', 'HIHI', 1, NULL, 'co', NULL),
	(15, 'SAN PHAM 1FDF', 500, '1.jpg', 'HIHI', 1, NULL, NULL, NULL),
	(16, 'SAN PHAM 143', 500, '1.jpg', 'HIHI', 1, NULL, 'co', NULL),
	(17, 'T', 500, '1.jpg', 'HIHI', 1, NULL, NULL, NULL),
	(18, 'SAN PHAM 1432', 500, '1.jpg', 'HIHI', 1, NULL, NULL, NULL),
	(19, 'SAN PHAM 1TDFRTE', 500, '1.jpg', 'HIHI', 1, NULL, NULL, NULL),
	(20, 'SAN PHAM 14324', 500, '1.jpg', 'HIHI', 1, NULL, NULL, NULL);
/*!40000 ALTER TABLE `san_pham` ENABLE KEYS */;

-- Dumping structure for table da-php-first.slideshow
CREATE TABLE IF NOT EXISTS `slideshow` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `HINH` varchar(256) DEFAULT NULL,
  `LIEN_KET` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table da-php-first.slideshow: ~5 rows (approximately)
/*!40000 ALTER TABLE `slideshow` DISABLE KEYS */;
INSERT INTO `slideshow` (`ID`, `HINH`, `LIEN_KET`) VALUES
	(1, 'a_1.png', '#'),
	(2, 'a_2.png', '#'),
	(3, 'a_3.png', '#'),
	(4, 'a_4.png', '#'),
	(5, 'a_1.png', '#');
/*!40000 ALTER TABLE `slideshow` ENABLE KEYS */;

-- Dumping structure for table da-php-first.thong_tin_quan_tri
CREATE TABLE IF NOT EXISTS `thong_tin_quan_tri` (
  `ID` int(11) NOT NULL,
  `KY_DANH` varchar(256) NOT NULL,
  `MAT_KHAU` varchar(256) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table da-php-first.thong_tin_quan_tri: ~0 rows (approximately)
/*!40000 ALTER TABLE `thong_tin_quan_tri` DISABLE KEYS */;
INSERT INTO `thong_tin_quan_tri` (`ID`, `KY_DANH`, `MAT_KHAU`) VALUES
	(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');
/*!40000 ALTER TABLE `thong_tin_quan_tri` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
